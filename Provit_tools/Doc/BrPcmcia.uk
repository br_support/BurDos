------------------------------------------------------------------------
File:          BrPcmcia.sys
Date:          24.07.2000
Description:   Device driver for IPC2000/2001 PCMCIA FProm and 
               PCMCIA SRAM cards 
------------------------------------------------------------------------
	
	The device driver can only handle one slot.
	Parameters can be set for the type, map window, etc.. 

	Call: device=brpcmcia.sys /Size:1024 /Slot:1 /Type:SRAM

		Here, the PCMCIA device driver is operated in slot 1.
		SRAM cards are supported and the size is maximum 
		1 Mbyte.

	Parameter       /SIZE:nnnn      The values are output in decimal 
					and define the size in Kbytes. 

			/Slot:n         1 or 2. Slot number

			/Type:[key]     'SRAM' or 'FPROM'. This setting is 
					important so that data can be read 
					and written properly (FProm burning).

			/Base:nnnn      Map window e.g. D000. The map window
					is only 4 Kbytes in size. If an EMM386 
					is started, this area must be excluded.

			/WP:[key]       'ON' or 'OFF'. Switches the software 
					write protect on or off.

		NEW Version >= 2:42
			/Slot:B		Serach automatic the PCMCIA boot slot

			/OFF:		Is the physical offset in kilobyte

			/PWIN:		Set the page register window.
					Settings between 1 to 5.

	Note:                                   

	The drive assignment is done automatically.

	No all parameters have to be defined. The following
	settings are default:
		/SIZE = 1024
		/Slot = 1
		/Type = SRAM
		/Base = ce00
		/WP   = Off
	
	A multiple of 512 Kbytes can be defined for a Flash card. 
	If a number that is not a multiple of 512 is defined, this is
	rounded to the nearest multiple respectively.


	There are several limitations for Flash cards:
	---------------------------------------------------
	- Copy          Copying to the same file causes an error, since the 
			old FAT should be deleted when the target file is
			opened. The FAT cannot be burnt back to 0xff.

	- Attrib        Hidden is only possible from the archive file attribute.
			Reversing the operation is not possible.

	- rename        File names can only be changed conditionally. 
			'test.exe' to 'test1.exe' is okay, however
			'test.exe' to 'abcd.exe' causes an error.

	- subdirectory  Up to 16 directory entries can be defined within a 
			subdirectory. Starting at the 17th entry, DOS tries
			to burn a byte in the FAT area to 0xff.

	- smartdrv      smartdrv must be switched off for this device
			 (Command: "smartdrv [drivenr]-").

	- Windows       Most windows programs make a temp file when saving. 
			Temp files are not supported on this device.

	- Writing single sectors.       In order to write to a single sector
					(like label.exe), DOS-Support must be  
					switched off (with an interrupt) 
					previously.

	- del           Data cannot be deleted with "del". Deleting is only  
			possible by reformatting the entire FProm.

	More information can be found in the technical documentation for
	Provit 2000".

Copyright (c) by B&R
------------------------------------------------------------------------
Bernecker & Rainer, Industrie Elektronik Ges.m.b.H
A-5142 Eggelsberg 120, Austria
Tel: +43 7748 6586 0
Fax: +43 7748 6586 26
Web: http://www.br-automation.com
------------------------------------------------------------------------